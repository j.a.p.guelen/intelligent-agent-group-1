package com.company;

import java.io.File;

//hermit
import javafx.util.Pair;
import org.semanticweb.HermiT.*;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.*;
import org.semanticweb.owlapi.util.ShortFormProvider;
import org.semanticweb.owlapi.util.SimpleShortFormProvider;
import org.json.*;
//import de.derivo.sparqldlapi.*;

import java.io.*;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

public class Main {

    //public static String lastPageVisited = null;
    public static OWLNamedIndividual lastProblem = null;
    public static Reasoner hermit = null;
    public static DLQueryEngine queryEngine;

    public static void printLine(String line, int[] commentIndex){
        System.out.println("\"" + commentIndex[0] + "\":\"" + line + "\",");
        commentIndex[0]--;
    }


    public static OWLClass domainClass;

    public static void main(String[] args) {

        int[] commentIndex = {-1};

        // write your code here
        String inputFile = "../Ontology/Ontology_Individuals.owl";
        if(args.length < 1){
            printLine("need input ontology file location", commentIndex);
            printLine("using: " + inputFile, commentIndex);
        }else{
            inputFile = args[0];
        }
        //System.out.println(args[0]);

        //*
        // First, we create an OWLOntologyManager object. The manager will load and
        // save ontologies.
        OWLOntologyManager manager=OWLManager.createOWLOntologyManager();
        // Now, we create the file from which the ontology will be loaded.
        // Here the ontology is stored in a file locally in the ontologies subfolder
        // of the examples folder.
        File inputOntologyFile = new File(inputFile);


        // We use the OWL API to load the ontology.
        OWLOntology ontology;
        try {
            ontology = manager.loadOntologyFromOntologyDocument(inputOntologyFile);
        }catch(OWLOntologyCreationException oe){
            printLine("file: '"  + inputOntologyFile + "' not found", commentIndex);
            return;
        }

        // Now we create a configuration object that we use to overwrite HermiT's default
        // settings.
        Configuration config=new Configuration();
        // Now we can start and create the reasoner with the above created configuration.
        hermit = new Reasoner(config,ontology);// Entities are named using IRIs. These are usually too long for use
        // in user interfaces. To solve this
        // problem, and so a query can be written using short class,
        // property, individual names we use a short form
        // provider. In this case, we'll just use a simple short form
        // provider that generates short froms from IRI
        // fragments.
        ShortFormProvider shortFormProvider = new SimpleShortFormProvider();

        // Create the DLQueryPrinter helper class. This will manage the
        // parsing of input and printing of results
        queryEngine = new DLQueryEngine(hermit, shortFormProvider);
        //QueryEngine SPARQLDL = QueryEngine.create(manager, hermit);

        //printLine("opened file: '" + inputOntologyFile + "'", commentIndex);

        OWLDataFactory dataFactory=manager.getOWLDataFactory();

        Set<OWLClass> domainClassSet = queryEngine.getEquivalentClasses("hasDomain some ProblemDomain");
        if(!domainClassSet.isEmpty()){
            for(OWLClass c : domainClassSet){
                domainClass = c;
                break;
            }
        }

        /*
        ///query for testing DL
        Set<OWLNamedIndividual> individuals =  DLQueryEngine.getInstances("Problem", false);
        printEntities(individuals, shortFormProvider);
        //*/

        /*
        String prefixString = "PREFIX : <http://www.co-ode.org/ontologies/IntelligentAgentsProgramming#>\n" +
                "PREFIX owl: <http://www.w3.org/2002/07/owl#>\n" +
                "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
                "PREFIX xml: <http://www.w3.org/XML/1998/namespace>\n" +
                "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>\n" +
                "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n";
        ///query for testing SPARQL-DL
        try {
            String q =  prefixString +
                    "SELECT ?y " +
                    "WHERE " +
                    "{ " +
                    "   PropertyValue(?i, :hasURL, \"https://www.hackerrank.com/challenges/a-chessboard-game-1/problem\") " +
                    "}";
            Query query = Query.create(q);
            QueryResult result = SPARQLDL.execute(query);


            if(result.ask()){
                printLine("results:", commentIndex);
                printLine(result.toString(), commentIndex);
            }else{
                printLine("no results found", commentIndex);
            }
        }catch(Exception e){
            printLine("Invalid query",commentIndex);
            printLine(e.toString(), commentIndex);
        }
        //*/



        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        //System.out.print("Hello. Please write your name: ");
        String input = "";
        while(!input.equals("q")) {
            try {
                input = br.readLine();
            } catch (Exception e) {

                printLine("error: " + e, commentIndex);
            }

            JSONObject inputJSON;
            try{
                inputJSON = new JSONObject(input);
                //System.out.println(inputJSON.getInt("Type"));
                int typeNR = inputJSON.getInt("Type");

                String Data = "";
                if(typeNR != 0) {
                    Data=inputJSON.getString("Data");
                }
                int questionIndex;
                try{
                    questionIndex = inputJSON.getInt("Index");
                }catch(Exception e){
                    questionIndex = commentIndex[0];
                    commentIndex[0]--;
                }
                QueryOntologyByType(typeNR, Data, ontology, manager, dataFactory, questionIndex);
            }catch(Exception e){
                printLine(e.toString(), commentIndex);
            }

            //printLine("Your input was: " + input, commentIndex);
            //System.out.flush();
        }
        //*/
    }



    public static void QueryOntologyByType(int Type, String Data, OWLOntology ontology, OWLOntologyManager manager, OWLDataFactory dataFactory, int index){

        ShortFormProvider shortForm = new SimpleShortFormProvider();
        Set<OWLNamedIndividual> statuses = queryEngine.getInstances("ProblemStatus", true);
        OWLNamedIndividual unsolved = null;
        OWLNamedIndividual solved = null;
        OWLNamedIndividual abandoned = null;
        for(OWLNamedIndividual status : statuses) {
            if(shortForm.getShortForm(status).equals("UnsolvedStatus")){
                unsolved = status;
            }
            if(shortForm.getShortForm(status).equals("SolvedStatus")){
                solved = status;
            }
            if(shortForm.getShortForm(status).equals("AbandonedStatus")){
                abandoned = status;
            }
        }
        System.out.print("\"" + index + "\":");
        switch(Type){
            case 1:
                if(Data.contains("challenges")) {
                    Set<OWLNamedIndividual> individuals = queryEngine.getInstances("hasURL value \"" + Data + "\"", true);
                    if(!individuals.isEmpty()){
                        for(OWLNamedIndividual individual: individuals){
                            lastProblem = individual;
                            break;
                        }
                    }
                    System.out.print("\"set challenge\"");
                }else{
                    System.out.print("\"not in our problem set\"");
                }
                break;
            case 2:
                //System.out.println(lastPageVisited);
                if(lastProblem != null) {
                    OWLNamedIndividual lastIndiv = lastProblem;
                    Map<OWLObjectPropertyExpression, Set<OWLIndividual>> dataProperties = lastIndiv.getObjectPropertyValues(ontology);
                    for(Map.Entry<OWLObjectPropertyExpression, Set<OWLIndividual>> entry : dataProperties.entrySet()){
                        if(shortForm.getShortForm(entry.getKey().asOWLObjectProperty()).equals("hasStatus")){
                            for(OWLIndividual lit : entry.getValue()){
                                OWLNamedIndividual thisGuy = dataFactory.getOWLNamedIndividual(lastIndiv.getIRI());
                                if(shortForm.getShortForm(lit.asOWLNamedIndividual()).equals("UnsolvedStatus")) {
                                    OWLAxiom addAx = dataFactory.getOWLObjectPropertyAssertionAxiom(entry.getKey().asOWLObjectProperty(),thisGuy, abandoned);
                                    OWLAxiom removeAx = dataFactory.getOWLObjectPropertyAssertionAxiom(entry.getKey().asOWLObjectProperty(),thisGuy, lit);
                                    manager.removeAxiom(ontology, removeAx);
                                    manager.addAxiom(ontology, addAx);
                                }
                            }
                        }
                    }

                    Configuration config=new Configuration();
                    hermit = new Reasoner(config, ontology);
                    queryEngine = new DLQueryEngine(hermit, shortForm);

                }
                System.out.print("{}");
                break;
            case 3:
                if(lastProblem != null && Data.equals("True")) {
                    ShortFormProvider shortFormProvider = new SimpleShortFormProvider();
                    //set last problem to successful in ontology
                    Map<OWLObjectPropertyExpression, Set<OWLIndividual>> objectProperties = lastProblem.getObjectPropertyValues(ontology);
                    OWLNamedIndividual thisGuy = dataFactory.getOWLNamedIndividual(lastProblem.getIRI());
                    for (Map.Entry<OWLObjectPropertyExpression, Set<OWLIndividual>> entry : objectProperties.entrySet()) {
                        if (shortFormProvider.getShortForm(entry.getKey().asOWLObjectProperty()).equals("hasStatus")) {
                            for (OWLIndividual lit : entry.getValue()) {
                                OWLAxiom addAx = dataFactory.getOWLObjectPropertyAssertionAxiom(entry.getKey().asOWLObjectProperty(),thisGuy, solved);
                                OWLAxiom removeAx = dataFactory.getOWLObjectPropertyAssertionAxiom(entry.getKey().asOWLObjectProperty(),thisGuy, lit);
                                manager.removeAxiom(ontology, removeAx);
                                manager.addAxiom(ontology, addAx);
                            }
                        }
                    }


                    Configuration config=new Configuration();
                    hermit = new Reasoner(config, ontology);
                    queryEngine = new DLQueryEngine(hermit, shortForm);
                }

                System.out.print("{}");
                break;
            case 4:
                if(lastProblem != null) {
                    ShortFormProvider shortFormProvider = new SimpleShortFormProvider();
                    //set start for last problem
                    Map<OWLDataPropertyExpression, Set<OWLLiteral>> dataProperties = lastProblem.getDataPropertyValues(ontology);
                    for(Map.Entry<OWLDataPropertyExpression, Set<OWLLiteral>> entry : dataProperties.entrySet()){
                        if(shortFormProvider.getShortForm(entry.getKey().asOWLDataProperty()).equals("hasStars")){
                            //set owl literal to integer in data
                        }
                    }
                }
                //git try what bryan said
                System.out.print("{}");
                break;
            default:
                System.out.print("[");
                returnValidProblems(queryEngine, ontology);
                System.out.print("]");
                break;

        }
        System.out.print(",\n");
    }

    public static void returnValidProblems( DLQueryEngine manager, OWLOntology ontology){
        //lastPageVisited = "https://www.hackerrank.com/challenges/a-chessboard-game-1/problem";


        ShortFormProvider shortFormProvider = new SimpleShortFormProvider();
        Set<OWLNamedIndividual> individuals =  manager.getInstances("hasValidity value ValidProblem", false);// and hasDomain value StringsDomain", false);//
        LinkedList<Integer> depthDifferenceListCurrentProblem = new LinkedList<>();
        LinkedList<Integer> difficultyDifferenceListCurrentProblem = new LinkedList<>();




        //calculate score items based on user preference
        //calculate the depth difference for each individual
        Set<OWLNamedIndividual> tempUsers = manager.getInstances("{User}", false);
        OWLNamedIndividual user = null;
        for(OWLNamedIndividual tInd : tempUsers){
            user = tInd;
            break;
        }
        //get difficulty
        Boolean medium = false;
        Boolean hard = false;
        Map<OWLObjectPropertyExpression, Set<OWLIndividual>> objectProperties = user.getObjectPropertyValues(ontology);
        for(Map.Entry<OWLObjectPropertyExpression, Set<OWLIndividual>> entry : objectProperties.entrySet()){
            if(shortFormProvider.getShortForm(entry.getKey().asOWLObjectProperty()).equals("hasUserDifficulty")){
                for(OWLIndividual lit : entry.getValue()){
                    if(shortFormProvider.getShortForm(lit.asOWLNamedIndividual()).equals("UserMediumDifficulty")){
                        medium = true;
                    }
                    if(shortFormProvider.getShortForm(lit.asOWLNamedIndividual()).equals("UserHardDifficulty")){
                        hard = true;
                    }
                }
            }
        }
        //assume easy
        int preferredDifficulty = 0;
        if(hard){
            preferredDifficulty = 2;
        }else if(medium){
            preferredDifficulty = 1;
        }



        LinkedList<OWLClass> ClassesTillDomain = new LinkedList<>();
        LinkedList<Integer> preferredDifficultyList = new LinkedList<>();
        LinkedList<Integer> depthMapPreferred = new LinkedList<>();
        OWLClass preferenceClass = null;
        OWLClass SuperClass;
        if(preferenceClass != null){
            SuperClass = preferenceClass;
            ClassesTillDomain.add(SuperClass);
            do{
                SuperClass = nextDomainSuperClass(shortFormProvider.getShortForm(SuperClass), manager,
                        domainClass, shortFormProvider);
                ClassesTillDomain.add(SuperClass);
            }while(SuperClass != domainClass);
        }
        OWLIndividual classIndividual = null;

        Boolean lastProblemSucceeded = false;

        if(lastProblem != null && domainClass != null){
            //get the list for the current problem in advance, so we can use it multiple times
            LinkedList<OWLClass> currentProblemClassesTillDomain = new LinkedList<>();


            /*
            SuperClass = nextDomainSuperClass("{" + shortFormProvider.getShortForm(lastProblem) + "}", manager,
                    domainClass, shortFormProvider);
            currentProblemClassesTillDomain.add(SuperClass);
            do {
                SuperClass = nextDomainSuperClass(shortFormProvider.getShortForm(SuperClass), manager,
                        domainClass, shortFormProvider);

                currentProblemClassesTillDomain.add(SuperClass);
            }while(!SuperClass.equals(domainClass));
            */
            //see if last problem was successful
            objectProperties = lastProblem.getObjectPropertyValues(ontology);
            for(Map.Entry<OWLObjectPropertyExpression, Set<OWLIndividual>> entry : objectProperties.entrySet()){
                if(shortFormProvider.getShortForm(entry.getKey().asOWLObjectProperty()).equals("hasStatus")){
                    for(OWLIndividual lit : entry.getValue()){
                        if(shortFormProvider.getShortForm(lit.asOWLNamedIndividual()).equals("SolvedStatus")){
                            lastProblemSucceeded = true;
                        }
                    }
                }else if(shortFormProvider.getShortForm(entry.getKey().asOWLObjectProperty()).equals("hasDomain")){
                    for(OWLIndividual lit : entry.getValue()){
                        classIndividual = lit;
                        /*
                        Set<OWLClass> classSet = manager.getSuperClasses("{" + shortFormProvider.getShortForm(lit.asOWLNamedIndividual()) + "}", true);
                        for(OWLClass sc : classSet) {
                            currentProblemClassesTillDomain.add(sc);
                        }
                        */
                    }
                }

            }

            int currentProblemDifficulty = getDifficulty(lastProblem, manager, shortFormProvider);

            //calculate the depth difference for each individual
            for(OWLNamedIndividual ind : individuals){
                int counter = 0;
                objectProperties = ind.getObjectPropertyValues(ontology);
                for(Map.Entry<OWLObjectPropertyExpression, Set<OWLIndividual>> entry : objectProperties.entrySet()){
                    if(shortFormProvider.getShortForm(entry.getKey().asOWLObjectProperty()).equals("hasStatus")){
                        for(OWLIndividual lit : entry.getValue()){
                            if(shortFormProvider.getShortForm(lit.asOWLNamedIndividual()).equals("SolvedStatus")){
                                lastProblemSucceeded = true;
                            }
                        }
                    }else if(shortFormProvider.getShortForm(entry.getKey().asOWLObjectProperty()).equals("hasDomain")){
                        for(OWLIndividual lit : entry.getValue()){
                            if(shortFormProvider.getShortForm(lit.asOWLNamedIndividual()).equals(
                                    shortFormProvider.getShortForm(classIndividual.asOWLNamedIndividual())
                            )){
                                counter = 1;
                            }
                        }
                    }

                }


                /*
                SuperClass = nextDomainSuperClass("{" + shortFormProvider.getShortForm(ind) + "}", manager,
                        domainClass, shortFormProvider);
                for(OWLClass cpClass : currentProblemClassesTillDomain){
                    if(SuperClass.equals(cpClass)){
                        break;
                    }
                    counter++;
                    SuperClass = nextDomainSuperClass(shortFormProvider.getShortForm(SuperClass), manager,
                            domainClass, shortFormProvider);
                }
                */

                //inverse both values, because higher score is better, and that is easier with a times;
                //depthDifferenceListCurrentProblem.add(currentProblemClassesTillDomain.size() - counter);

                depthDifferenceListCurrentProblem.add(2 - counter);
                int differenceDiff = currentProblemDifficulty - getDifficulty(ind, manager, shortFormProvider);
                if(differenceDiff < 0 && lastProblemSucceeded){
                    differenceDiff*=2;
                }
                difficultyDifferenceListCurrentProblem.add(4 - Math.abs(differenceDiff));
            }


        }



        for(OWLNamedIndividual ind : individuals){
            /*
            int counter = 0;
            SuperClass = nextDomainSuperClass("{" + shortFormProvider.getShortForm(ind) + "}", manager,
                    domainClass, shortFormProvider);
            for (OWLClass cpClass : ClassesTillDomain) {
                if (SuperClass.equals(cpClass)) {
                    break;
                }
                counter++;
                SuperClass = nextDomainSuperClass(shortFormProvider.getShortForm(SuperClass), manager,
                        domainClass, shortFormProvider);
            }
            //*/
            //inverse both values, because higher score is better, and that is easier with a times;
            //depthMapPreferred.add(ClassesTillDomain.size() - counter);
            preferredDifficultyList.add(3 - Math.abs(preferredDifficulty - getDifficulty(ind, manager, shortFormProvider)));
        }



        //show the score based on the depthDifferenceLists;
        StringBuilder sb = new StringBuilder();
        int i = 0;
        for (OWLNamedIndividual entity : individuals) {
            if(sb.length() != 0){
                sb.append("\n");
                sb.append(",");
            }
            sb.append("{");
            sb.append("\"Name\":\"");
            sb.append(shortFormProvider.getShortForm(entity));
            sb.append("\",\"Score\":");
            sb.append("[");
            sb.append((depthDifferenceListCurrentProblem.size() > i ? depthDifferenceListCurrentProblem.get(i) * 5 : 0));
            sb.append(",");
            sb.append(difficultyDifferenceListCurrentProblem.size() > i ? difficultyDifferenceListCurrentProblem.get(i) * 3.2 : 0);
            /*
            sb.append(",");
            sb.append(depthMapPreferred.get(i) * 6);
            */
            sb.append(",");
            sb.append(preferredDifficultyList.get(i) * 1.5);

            //see if problem was abandoned
            Boolean abandoned = false;
            objectProperties = entity.getObjectPropertyValues(ontology);
            for(Map.Entry<OWLObjectPropertyExpression, Set<OWLIndividual>> entry : objectProperties.entrySet()){
                if(shortFormProvider.getShortForm(entry.getKey().asOWLObjectProperty()).equals("hasStatus")){
                    for(OWLIndividual lit : entry.getValue()){
                        if(shortFormProvider.getShortForm(lit.asOWLNamedIndividual()).equals("AbandonedStatus")){
                            abandoned = true;
                        }
                    }
                }
            }
            sb.append(",");
            sb.append((abandoned ? 0 : 1) * 9);
            //get the rating
            Map<OWLDataPropertyExpression, Set<OWLLiteral>> dataProperties = entity.getDataPropertyValues(ontology);
            for(Map.Entry<OWLDataPropertyExpression, Set<OWLLiteral>> entry : dataProperties.entrySet()){
                String sF = shortFormProvider.getShortForm(entry.getKey().asOWLDataProperty());
                if(sF.equals("hasSuccesRate")){
                    for(OWLLiteral lit : entry.getValue()){
                        sb.append(",");
                        sb.append((lit.parseInteger()/10000.0f) *2.0f);
                    }
                }
            }

            sb.append("]");


            for(Map.Entry<OWLDataPropertyExpression, Set<OWLLiteral>> entry : dataProperties.entrySet()){
                String sF = shortFormProvider.getShortForm(entry.getKey().asOWLDataProperty());
                if(sF.equals("hasURL")){
                    for(OWLLiteral lit : entry.getValue()){
                        String litString = lit.toString();
                        sb.append(",\"URL\":");
                        sb.append(litString.replace("^^xsd:string", ""));
                    }
                }
            }
            sb.append("}");
            i++;
        }

        System.out.println(sb.toString());
    }

    public static int getDifficulty(OWLNamedIndividual currentProblem, DLQueryEngine manager, ShortFormProvider shortFormProvider){
        Set<OWLClass> superClasses = manager.getSuperClasses("{" + shortFormProvider.getShortForm(currentProblem) + "}", true);
        for(OWLClass c : superClasses){
            switch (shortFormProvider.getShortForm(c)){
                case "Easy":
                    return 0;
                case "Medium":
                    return 1;
                case "Hard":
                    return 2;
                default:
                    break;
            }
        }
        return -1;
    }


    public static OWLClass nextDomainSuperClass(String currentDomainSuperClass, DLQueryEngine manager,
                                                OWLClass domainSuperClass, ShortFormProvider shortFormProvider){
        Set<OWLClass> superClasses = manager.getSuperClasses(currentDomainSuperClass, true);
        for(OWLClass OC : superClasses){
            Set<OWLClass> OCSuperClasses = manager.getSuperClasses(shortFormProvider.getShortForm(OC), false);
            if(OCSuperClasses.contains(domainSuperClass)){
                return OC;
            }
        }
        return domainSuperClass;
    }


    public static void printEntities(Set<? extends OWLEntity> entities, ShortFormProvider  shortFormProvider) {

        StringBuilder sb = new StringBuilder();
        if (!entities.isEmpty()) {
            for (OWLEntity entity : entities) {
                if(sb.length() != 0){
                    sb.append(",");
                }
                sb.append("\"");
                sb.append(shortFormProvider.getShortForm(entity));
                sb.append("\"");
                sb.append("\n");
            }
        } else {
            sb.append("\t\"[NONE]\"\n");
        }
        sb.append("\n");

        System.out.println(sb.toString());


    }




}
