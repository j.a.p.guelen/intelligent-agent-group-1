//js of the popup when clicked
// basicly our view 
var jokes = ["What do you call a Mexican who has lost his car? Carlos.","I would avoid the sushi if I was you. Its a little fishy.","Whats brown and sticky? A stick.","How does a penguin build its house? Igloos it together.","Whats Forrest Gumps password? 1forrest1","I dont play soccer because I enjoy the sport. Im just doing it for kicks.","How do you organize an outer space party? You planet."," What do you call a belt with a watch on it? A waist of time.","Did you hear about the man who stole a calendar? He got 12 months.","Without geometry life is pointless.","What noise does a 747 make when it bounces? Boeing, Boeing, Boeing."];




var clippyagent;
async function asyncCall() {
    clippy.load('Clippy', function(agent){
			// Do anything with the loaded agent
		clippyagent = agent;
		agent.show();
		
		//agent.moveTo(0,0);
		//document.getElementsByClassName("clippy")[0].style.width = "250px";
		
		//document.getElementsByClassName("clippy")[0].style.height = "250px";
		document.getElementsByClassName("clippy")[0].style.left = "350px";
		document.getElementsByClassName("clippy")[0].style.top = "50px";
		

		
		
		
		
		//agent.speak('When all else fails, bind some paper together. My name is Clippy.');
		//agent.animate();
     chrome.runtime.getBackgroundPage((page) =>
     {
		if(clippyagent){
			
            clippyagent.speak("Please wait. Finding most suitable problems for you.");
			r = Math.floor(Math.random() * 2);
			console.log(r)
			if( r == 0){
			clippyagent.play("CheckingSomething");
			
			}
			clippyagent.play("Processing");
			clippyjoke(clippyagent);
			clippyagent.play("CheckingSomething");
			clippyagent.play("Processing");
			clippyjoke(clippyagent);
			clippyagent.play("CheckingSomething");
			clippyagent.play("Processing");
			clippyjoke(clippyagent);
			clippyagent.play("CheckingSomething");
			clippyjoke(clippyagent);
			clippyagent.play("Processing");
			clippyagent.play("CheckingSomething");
			clippyagent.play("Processing");
			clippyagent.play("CheckingSomething");
			clippyagent.play("Processing");
			clippyagent.play("CheckingSomething");
			clippyagent.play("Processing");
			clippyagent.play("CheckingSomething");
			clippyagent.play("Processing");
			clippyagent.play("CheckingSomething");
			
            page.getNextResponse((data) => {
                //page.console.log("converting to Array");
                var dataArray = Array.from(data);
                //page.console.log(dataArray);
                
                dataArray.forEach((el) => {
                    var tScore = 0;
                    Array.from(el["Score"]).forEach((el2) => { tScore += el2 });
                    el.totalScore = tScore;
                    //page.console.log(tScore);
                })

                dataArray.sort((dp1, dp2) =>  dp2.totalScore - dp1.totalScore);
                //page.console.log(dataArray);
                var showSet = dataArray.slice(0, Math.min(3, dataArray.length));
                //page.console.log(showSet);
                var showLabels = [];
                for (var i = 0; i < showSet.length; i++) {
                    showLabels[i] = showSet[i]["Name"];
                    linkMap[showSet[i]["Name"]] = showSet[i]["URL"];
                }
                //page.console.log(showLabels);
                //page.console.log(myChart);
                myChart.data.labels = showLabels;
                for (var i = 0; i < myChart.data.datasets.length; i++) {
                    var dataValues = [];
                    for (var j = 0; j < showSet.length; j++) {                            
                        dataValues[j] = +showSet[j]["Score"][i].toFixed(3);
                    }
                    myChart.data.datasets[i].data = dataValues;
                }
                //page.console.log(myChart);
                myChart.update();
				
				clippyagent.stop();
				clippyagent.stopCurrent();
                clippyagent.speak("I suggest you try one of the following problems: " + showLabels.join(",\n"));
				clippyagent.play("SendMail");
            });
                

            ///page.getNextResponse();

			//setTimeout(speak,500);
				
				
		}	
	});
		
		
	});
}

function clippyjoke(clippy){
	
	
	r = Math.floor(Math.random() * jokes.length);
	
	clippy.speak(jokes[r]);
	
	
	
}

var linkMap = {};

var myChart;

document.addEventListener('DOMContentLoaded', function (event) {
	document.body.style.minWidth = "500px";
	document.body.style.minHeight =	"500px";
	

    var ctx = document.getElementById("myChart");
    myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: [],
            datasets: [{
                label: 'Catagory score (previous problem)',
                data: [],
                backgroundColor: 'rgba(255, 99, 132, 0.2)',
                    borderColor: 'rgba(255, 99, 132, 1)',
                borderWidth: 1
            },
                {
                label: 'Difficulty score (previous problem)',
                data: [],
                backgroundColor:  'rgba(54, 162, 235, 0.2)',
                    borderColor:  'rgba(54, 162, 235, 1)',
                borderWidth: 1
            },
             /*   {
                label: 'Catagory score (UserPreference)',
                data: [],
                backgroundColor:  'rgba(162, 235, 54, 0.2)',
                    borderColor:  'rgba(162, 235, 54, 1)',
                borderWidth: 1
            },*/
                {
                label: 'Difficulty score (UserPreference)',
                data: [],
                backgroundColor:  'rgba(1, 1, 1, 0.2)',
                    borderColor:  'rgba(1, 1, 1, 1)',
                borderWidth: 1
            },
                {
                label: 'Unsolved or Abandoned',
                data: [],
                backgroundColor:  'rgba(255, 255, 255, 0.2)',
                    borderColor:  'rgba(0, 0, 0, 1)',
                borderWidth: 1
                },
            {
                label: 'SuccessRate',
                data: [],
                backgroundColor: 'rgba(255, 0, 255, 0.2)',
                borderColor: 'rgba(255, 0, 255, 1)',
                borderWidth: 1
                }]
        },
        options: {
            scales: {
                yAxes: [{
                    stacked: true,
                    ticks: {
                        beginAtZero: true
                    }
                }],
                xAxes: [{
                    stacked: true,
                }]
            },
            tooltips: {
                mode: 'index',
                intersect: false
            }
        }
    });


    $("#myChart").click(
        function (evt) {
            console.log("found click");
            var activePoints = myChart.getElementsAtEvent(evt);
            if (activePoints[0] === undefined) return;
            var chartData = activePoints[0]['_chart'].config.data;
            var idx = activePoints[0]['_index'];

            var label = chartData.labels[idx];
            var value = chartData.datasets[0].data[idx];
            onPickElement(label);
        }
    );  
	
});


function onPickElement(label) {
    //console.log(label);
    console.log(linkMap[label]);
    window.open(linkMap[label]);
}


window.addEventListener("load", function(event){
	
	asyncCall();
});