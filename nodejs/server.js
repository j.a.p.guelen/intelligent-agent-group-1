var express = require('express'),
  app = express(),
    port = process.env.PORT || 3000;

var bodyParser = require('body-parser');

////////////////////////The java process
const { spawn } = require('child_process');
const child = spawn('java', ['-jar', '../HermitApp/out/artifacts/HermitApp_jar/HermitApp.jar']);

child.on('error', (err) => {
    console.log('Failed to start subprocess');
})

var returnData = "";
child.stdout.on('data', (data) => {
    returnData += data;
    //console.log("data: " + data);
})

child.stderr.on('data', (data) => {
    console.log("error: " + data);
});

child.on('close', (code) => {
    console.log("close: " + code);
});
///////////////////////////////////////////


var requestIndex = 0;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

function returnDataJSONString() {
    //console.log("\n\n\n" + returnData.trim() + " =====> \n" + returnData.trim().slice(0, returnData.trim().length -1) + "\n\n\n");
    return "{" + returnData.trim().slice(0, returnData.trim().length - 1) + "}";
}

function recursiveWait(resolve, reject, index) {
    var validParse = true;
    try {
        var dataObject = JSON.parse(returnDataJSONString());
    } catch (error) {
        validParse = false;
    }   
    //console.log(validParse + "_" + index);
    if (!(validParse && dataObject.hasOwnProperty(index))) {
        var innerP = new Promise(
            (innerResolve, innerReject) =>
                setTimeout(() => {
                    //console.log("timeout complete");
                    //console.log("data now: " + returnData);
                    recursiveWait(innerResolve, innerReject, index)
                }, 1000)
        ).then(() => {
            //console.log("InnerResolve");
            resolve();
        });
    } else {
        //console.log("Returndata: {" + returnData + "}");
        resolve();
    }
}

app.get('/message', async (req, res) => {
    console.log("getting problems");
    var thisIndex = requestIndex;
    requestIndex++;
    child.stdin.write(JSON.stringify({ "Type": 0, "Index": thisIndex }) + "\n");	
    var p = new Promise((resolve, reject) => recursiveWait(resolve, reject, thisIndex)).then(() => {
        var dataObject = JSON.parse(returnDataJSONString());
        res.send(dataObject[thisIndex]);
        console.log("getData: {" + JSON.stringify(dataObject[thisIndex]) + "}");
        delete dataObject[thisIndex];
        var JSString = JSON.stringify(dataObject);
        returnData = JSString.slice(1, JSString.length-1) +",";
    })
})

app.post('/message', async (req, res) => {
    console.log("updating backend");
    var thisIndex = requestIndex;
    requestIndex++;
    req.body.message["Index"] = thisIndex;
    var message = JSON.stringify(req.body.message) + "\n" 
    child.stdin.write(message);
    var p = new Promise((resolve, reject) => recursiveWait(resolve, reject, thisIndex)).then(() => {
        //console.log(returnData);
        var dataObject = JSON.parse(returnDataJSONString());
        res.send(dataObject[thisIndex]);
        console.log("message: " + message + "backData: {" + JSON.stringify(dataObject[thisIndex]) + "}");

        delete dataObject[thisIndex];
        var JSString = JSON.stringify(dataObject);
        //console.log(JSString.slice(1, JSString.length - 1) + ",");
        returnData = JSString.slice(1, JSString.length - 1) + ",";
    })
})



/*
var routes = require('./routes/routing'); //importing route
routes(app); //register the route
*/

app.listen(port);


console.log('todo list RESTful API server started on: ' + port);


