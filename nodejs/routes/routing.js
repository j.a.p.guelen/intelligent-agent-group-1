'use strict';
module.exports = function(app) {
  var todoList = require('../controller/controller');

  // todoList Routes
  app.route('/message')
    .get(todoList.getMessage)
    .post(todoList.sendMessage)


};
